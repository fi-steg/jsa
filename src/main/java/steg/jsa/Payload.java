/*
 * Copyright (c) 2018. Jesús Farfán
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package steg.jsa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * The interface Payload.
 *
 * @author Jesús Farfán
 */
public interface Payload {

  /**
   * Gets file.
   *
   * @return the file
   */
  File getFile();

  /**
   * Gets name.
   *
   * @return the name
   */
  String getName();

  /**
   * Gets mime type.
   *
   * @return the mime type
   */
  Optional<String> getMimeType();

  /**
   * Get content byte [ ].
   *
   * @return the byte [ ]
   * @throws IOException the io exception
   */
  byte[] getContent() throws IOException;

  InputStream getContentAsInputStream() throws FileNotFoundException;
  /**
   * Write content.
   *
   * @param filePathDestination the file path destination
   * @throws IOException the io exception
   */
  void writeContent(String filePathDestination) throws IOException;

  /**
   * Write content.
   *
   * @param fileDestination the file destination
   * @throws IOException the io exception
   */
  void writeContent(File fileDestination) throws IOException;
}
