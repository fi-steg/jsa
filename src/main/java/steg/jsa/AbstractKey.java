/*
 * Copyright (c) 2018. Jesús Farfán
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package steg.jsa;

import java.util.Objects;

/**
 * The type Abstract key.
 *
 * @author Jesús Farfán
 */
public abstract class AbstractKey implements Key {

  /**
   * The Secret.
   */
  protected String secret;

  /**
   * Instantiates a new Abstract key.
   */
  public AbstractKey() {
  }

  /**
   * Instantiates a new Abstract key.
   *
   * @param secret the secret
   */
  public AbstractKey(String secret) {
    setSecret(secret);
  }

  /**
   * Sets secret.
   *
   * @param secret the secret
   */
  protected void setSecret(String secret) {
    if (secret == null || secret.isEmpty()) {
      throw new IllegalArgumentException("Parameter secret is null or empty");
    }
    this.secret = secret;
  }

  /**
   * Gets secret.
   *
   * @return the secret
   */
  @Override
  public String getSecret() {
    return this.secret;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    AbstractKey that = (AbstractKey) obj;
    return Objects.equals(secret, that.secret);
  }

  @Override
  public int hashCode() {
    return Objects.hash(secret);
  }

  @Override
  public String toString() {
    return "AbstractKey{" +
        "secret='" + secret + '\'' +
        '}';
  }
}
