package steg.jsa;

public interface StegoMetaData {

  byte[] getContentAsByteArray();
}
