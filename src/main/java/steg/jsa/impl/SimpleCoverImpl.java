package steg.jsa.impl;

import java.io.File;
import steg.jsa.AbstractPayload;
import steg.jsa.SimpleCover;

public class SimpleCoverImpl extends AbstractPayload implements SimpleCover {


  /**
   * Instantiates a new File payload.
   *
   * @param file the file
   */
  public SimpleCoverImpl(File file) {
    super(file);
  }

  /**
   * Instantiates a new Generic payload.
   *
   * @param content the content
   */
  public SimpleCoverImpl(byte[] content) {
    super(content);
  }
}
