/*
 * Copyright (c) 2018. Jesús Farfán
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package steg.jsa.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Optional;
import steg.jsa.Payload;

/**
 * The type File payload.
 *
 * @author Jesús Farfán
 */
public class GenericPayload implements Payload {

  /**
   * The File.
   */
  private File file;
  private byte[] content;

  /**
   * Instantiates a new File payload.
   *
   * @param file the file
   */
  public GenericPayload(File file) {
    this.file = file;
  }

  /**
   * Instantiates a new Generic payload.
   *
   * @param content the content
   */
  public GenericPayload(byte[] content) {
    this.content = content;
  }

  /**
   * Gets file.
   *
   * @return the file
   */
  @Override
  public File getFile() {
    return this.file;
  }

  /**
   * Sets file.
   *
   * @param file the file
   */
  protected void setFile(File file) {
    if (file == null) {
      throw new IllegalArgumentException("File provided is null");
    }
    if (!file.exists()) {
      throw new IllegalArgumentException(
          "File provided [" + file.getAbsolutePath() + "] doesn't exist");
    }
    if (file.isDirectory()) {
      throw new IllegalArgumentException(
          "File provided [" + file.getAbsolutePath() + "] is a directory");
    }
    this.file = file;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  @Override
  public String getName() {
    return this.file.getName();
  }

  /**
   * Gets mime type.
   *
   * @return the mime type
   */
  @Override
  public Optional<String> getMimeType() {
    try {
      return Optional.ofNullable(Files.probeContentType(this.file.toPath()));
    } catch (IOException e) {
      return Optional.empty();
    }
  }

  /**
   * Get content byte [ ].
   *
   * @return the byte [ ]
   */
  @Override
  public byte[] getContent() throws IOException {
    if (content != null) {
      return this.content;
    }
    this.content = new byte[(int) this.file.length()];
    FileInputStream fileInputStream;
    try {
      fileInputStream = new FileInputStream(this.file);
      fileInputStream.read(this.content);
      fileInputStream.close();
      return this.content;
    } catch (FileNotFoundException e) {
      return null;
    }
  }

  @Override
  public InputStream getContentAsInputStream() throws FileNotFoundException {
    return new BufferedInputStream(new FileInputStream(this.file));
  }

  /**
   * Write content.
   *
   * @param filePathDestination the file path destination
   * @throws IOException the io exception
   */
  @Override
  public void writeContent(String filePathDestination) throws IOException {
    File file = new File(filePathDestination);
    this.writeContent(file);
  }

  /**
   * Write content.
   *
   * @param fileDestination the file destination
   * @throws IOException the io exception
   */
  @Override
  public void writeContent(File fileDestination) throws IOException {
    FileOutputStream outputStream = new FileOutputStream(fileDestination);
    outputStream.write(this.getContent());
  }
}
