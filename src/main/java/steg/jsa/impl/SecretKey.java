/*
 * Copyright (c) 2018. Jesús Farfán
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package steg.jsa.impl;

import steg.jsa.AbstractKey;
import steg.jsa.Key;

/**
 * The type Secret key.
 *
 * @author Jesús Farfán
 */
public class SecretKey extends AbstractKey implements Key {

  /**
   * Instantiates a new Secret key.
   */
  public SecretKey() {
  }

  /**
   * Instantiates a new Secret key.
   *
   * @param secret the secret
   */
  public SecretKey(String secret) {
    super(secret);
  }

  @Override
  public String toString() {
    return "SecretKey{" +
        "secret='" + secret + '\'' +
        '}';
  }
}
