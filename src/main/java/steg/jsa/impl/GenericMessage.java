package steg.jsa.impl;

import steg.jsa.AbstractMessage;
import steg.jsa.Message;
import steg.jsa.Payload;

import java.io.File;

/**
 * The type File message.
 */
public class GenericMessage extends AbstractMessage implements Message {

  /**
   * Instantiates a new File message.
   *
   * @param payload the payload
   */
  public GenericMessage(Payload payload) {
    super(payload);
  }


  public static final class Builder {
    protected Payload payload;

    private Builder() {
    }

    public static Builder aGenericMessage() {
      return new Builder();
    }

    public Builder withPayload(Payload payload) {
      this.payload = payload;
      return this;
    }

    public  Builder withFile(File file) {
      this.payload = new GenericPayload(file);
      return this;
    }

    public GenericMessage build() {
      return new GenericMessage(payload);
    }
  }
}
