/*
 * Copyright (c) 2018. Jesús Farfán
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package steg.jsa.impl;

import java.io.File;
import steg.jsa.AbstractCover;
import steg.jsa.Cover;
import steg.jsa.Payload;

/**
 * The type Generic cover.
 *
 * @author Jesús Farfán
 */
public class GenericCover extends AbstractCover implements Cover {

  /**
   * Instantiates a new File cover.
   *
   * @param payload the payload
   */
  public GenericCover(Payload payload) {
    super(payload);
  }

  /**
   * Instantiates a new File cover.
   *
   * @param file the file
   */
  public GenericCover(File file) {
    super(new GenericPayload(file));
  }

  public static final class Builder {
    protected Payload payload;

    private Builder() {
    }

    public static Builder aGenericCover() {
      return new Builder();
    }

    public Builder withPayload(Payload payload) {
      this.payload = payload;
      return this;
    }

    public Builder withFile(File file) {
      this.payload = new GenericPayload(file);
      return this;
    }

    public GenericCover build() {
      return new GenericCover(payload);
    }
  }
}
