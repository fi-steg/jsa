/*
 * Copyright (c) 2018. Jesús Farfán
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package steg.jsa.impl;

import steg.jsa.AbstractStego;
import steg.jsa.Payload;
import steg.jsa.Stego;
import steg.jsa.StegoMetaData;

/**
 * The type Generic stego.
 *
 * @author Jesús Farfán
 */
public class GenericStego extends AbstractStego implements Stego {
  private StegoMetaData stegoMetaData;

  /**
   * Instantiates a new Abstract stego.
   *
   * @param payload the payload
   */
  public GenericStego(Payload payload) {
    super(payload);
  }

  /**
   * Sets stego meta data.
   *
   * @param stegoMetaData the stego meta data
   */
  public void setStegoMetaData(StegoMetaData stegoMetaData) {
    this.stegoMetaData = stegoMetaData;
  }

  @Override
  public StegoMetaData getStegoMetaData() {
    return this.stegoMetaData;
  }
}
