package steg.jsa.impl;

import java.io.File;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import steg.jsa.Cover;
import steg.jsa.Payload;
import steg.jsa.Stego;

public class GenericStegoTest {

  @Test
  public void createGenericStego() throws IOException {
    Cover cover = new GenericCover(
        new File(getClass().getClassLoader().getResource("audio-cover.wav").getFile()));

    byte[] content = cover.getPayload().getContent();
    // After process modification, cover content is other
    Payload payload = new GenericPayload(content);
    Stego stego = new GenericStego(payload);
    Assert.assertNotNull(stego);

    File tempFile = File.createTempFile("stego", "temp");
    //stego.writePayload(tempFile);
    //stego.writePayload("audio-cover-stego.wav", "/tmp");
  }
}