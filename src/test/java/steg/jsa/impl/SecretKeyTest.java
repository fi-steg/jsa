package steg.jsa.impl;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SecretKeyTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  @Test
  public void secretKeyConstruct() {
    SecretKey secretKey = new SecretKey("MySecretKey");
    Assert.assertEquals("MySecretKey", secretKey.getSecret());
  }

  @Test
  public void emptySecretKeyThrowsInvalidArgumentException() {
    expectedException.expect(IllegalArgumentException.class);
    SecretKey secretKey = new SecretKey("");
    Assert.assertNull(secretKey);
  }

  @Test
  public void nullSecretKeyThrowsInvalidArgumentException() {
    expectedException.expect(IllegalArgumentException.class);
    SecretKey secretKey = new SecretKey(null);
    Assert.assertNull(secretKey);
  }
}