package steg.jsa.impl;

import java.io.File;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import steg.jsa.Payload;

public class GenericPayloadTest {

  @Test
  public void createGenericPayload() throws IOException {
    Payload payload = new GenericPayload(new File(getClass().getClassLoader().getResource("audio-cover.wav").getFile()));
    Assert.assertNotNull(payload);
    Assert.assertEquals("audio-cover.wav", payload.getName());
    Assert.assertEquals("audio/x-wav", payload.getMimeType().get());
    Assert.assertNotNull(payload.getContent());
  }

  @Test
  public void writeContent() throws IOException {
    Payload payload = new GenericPayload(new File(getClass().getClassLoader().getResource("audio-cover.wav").getFile()));
    File tempFile = File.createTempFile("audio-stego", ".temp");
    tempFile.deleteOnExit();
    payload.writeContent(tempFile);
    Assert.assertTrue(tempFile.exists());
    Assert.assertEquals(payload.getFile().length(), tempFile.length());
  }
}