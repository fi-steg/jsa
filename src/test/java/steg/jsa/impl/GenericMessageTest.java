package steg.jsa.impl;

import java.io.File;
import org.junit.Assert;
import org.junit.Test;
import steg.jsa.Payload;

public class GenericMessageTest {

  @Test
  public void createGenericMessage() {
    Payload payload = new GenericPayload(
        new File(getClass().getClassLoader().getResource("secret-message.txt").getFile()));
    GenericMessage message = new GenericMessage(payload);
    Assert.assertNotNull(message);
    Assert.assertEquals("secret-message.txt", message.getPayload().getName());
  }
}