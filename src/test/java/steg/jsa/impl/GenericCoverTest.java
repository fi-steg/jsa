package steg.jsa.impl;

import java.io.File;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;

/**
 * The type File cover test.
 */
public class GenericCoverTest {

  @Test
  public void CreateFileCover() throws IOException {
    ClassLoader classLoader = getClass().getClassLoader();
    GenericCover genericCover = new GenericCover(new File(classLoader.getResource("audio-cover.wav").getFile()));
    Assert.assertNotNull(genericCover);
    Assert.assertEquals("audio-cover.wav", genericCover.getPayload().getName());
    Assert.assertEquals("audio/x-wav", genericCover.getPayload().getMimeType().get());
    Assert.assertNotNull(genericCover.getPayload().getContent());
  }
}